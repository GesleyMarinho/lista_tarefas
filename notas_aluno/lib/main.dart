import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MaterialApp(
    home: Home(),
    debugShowCheckedModeBanner: false,
  ));
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController nota1Controller = TextEditingController();
  TextEditingController nota2Controller = TextEditingController();
  TextEditingController nota3Controller = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _infoText = "Aguardando as notas das Provas";

  void _soma() {
    setState(() {
      double a = double.parse(nota1Controller.text);
      double b = double.parse(nota2Controller.text);
      double c = double.parse(nota3Controller.text);
      double media = (a + b + c) / 3;
      print(media);

      if (media <= 5) {
        _infoText = " Você foi REPROVADO (${media.toStringAsPrecision(2)})";
      } else if (media <= 7) {
        _infoText =
            " Você está de RECUPERAÇÃO (${media.toStringAsPrecision(2)})";
      } else {
        _infoText = " Você foi APROVADO (${media.toStringAsPrecision(2)})";
      }
    });
  }

  void _reset() {
    nota1Controller.text = "";
    nota2Controller.text = "";
    nota3Controller.text = "";
    setState(() {
      _infoText = "Aguardando as notas das provas";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Simulador de Aprovação Escolar "),
        centerTitle: true,
        backgroundColor: Colors.red,
        actions: [
          IconButton(
            onPressed: _reset,
            icon: const Icon(Icons.refresh),
          )
        ],
      ),
      body: Container(
       height: MediaQuery.of(context).size.height,

        decoration: const BoxDecoration(
            gradient: LinearGradient(
                colors: [Colors.black, Colors.red, Colors.white])),
        //child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: const InputDecoration(
                        labelText: "Nota 1: ",
                        labelStyle: TextStyle(
                          color: Colors.red,
                          fontSize: 24.0,
                        ),
                      ),
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        color: Colors.black,
                        fontSize: 18.0,
                      ),
                      controller: nota1Controller,
                      validator: (String? value) {
                        if (value != null && value.isEmpty) {
                          return "Insira uma nota";
                        }
                        return null;
                      }),
                  TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: const InputDecoration(
                        labelText: "Nota 2: ",
                        labelStyle:
                            TextStyle(color: Colors.red, fontSize: 24.0),
                      ),
                      textAlign: TextAlign.center,
                      style:
                          const TextStyle(color: Colors.black, fontSize: 18.0),
                      controller: nota2Controller,
                      validator: (String? value) {
                        if (value != null && value.isEmpty) {
                          return "Insira uma nota";
                        }
                        return null;
                      }),
                  TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: const InputDecoration(
                        labelText: "Nota 3:",
                        labelStyle:
                            TextStyle(color: Colors.red, fontSize: 24.0),
                      ),
                      textAlign: TextAlign.center,
                      style:
                          const TextStyle(color: Colors.black, fontSize: 18.0),
                      controller: nota3Controller,
                      validator: (String? value) {
                        if (value != null && value.isEmpty) {
                          return "Insira uma nota";
                        }
                        return null;
                      }),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: RaisedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          _soma();
                        }
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Text(
                            "Calcular",
                            style:
                                TextStyle(color: Colors.black, fontSize: 48.0),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      color: Colors.red,
                    ),
                  ),
                  Text(
                    _infoText,
                    textAlign: TextAlign.center,
                    style: const TextStyle(color: Colors.black, fontSize: 24.0),
                  )
                ],
              ),
            ),
          ),
        ),
        //),
      ),
    );
  }
}
