import 'package:flutter/material.dart';

void main() {
  runApp(const MaterialApp(
    home: Home(),
    debugShowCheckedModeBanner: false,
  ));
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController weightController = TextEditingController();
  TextEditingController heightController = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _infotext = "Informe seus Dados ";

  void _resetFields() {
    weightController.text = "";
    heightController.text = "";
    setState(() {
      _infotext = "informe seus Dados ";
    });
  }

  void _calcular() {
    setState(() {
      double peso = double.parse(weightController.text);
      double altura = double.parse(heightController.text) / 100;
      double imc = peso / (altura * altura);
      print(imc);

      if (imc <= 18.5) {
        _infotext =
            "Abaixo do peso, 'Magreza' (${imc.toStringAsPrecision(3)}) ";
      } else if (imc <= 24.9) {
        _infotext = " Vc está com IMC, 'Normal' ";
      } else if (imc <= 29.9) {
        _infotext = "Acima do peso I, 'Sobrepeso' ";
      } else if (imc <= 39.9) {
        _infotext = " Acima do peso II, 'Obesidade' ";
      } else {
        _infotext = " Acima do Peso III, 'Obesidade Grave' ";
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Calculadora_IMC'),
        centerTitle: true,
        backgroundColor: Colors.lightBlue,
        actions: [
          IconButton(
            onPressed: _resetFields,
            icon: Icon(Icons.refresh),
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
          padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const Icon(
                  Icons.person_outline,
                  size: 160.0,
                  color: Colors.blue,
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(
                      labelText: "Peso KG",
                      labelStyle:
                          TextStyle(color: Colors.blueAccent, fontSize: 24.0)),
                  textAlign: TextAlign.center,
                  style:
                      const TextStyle(color: Colors.blueAccent, fontSize: 24.0),
                  controller: weightController,
                  validator: (String? value) {
                    if (value != null && value.isEmpty) {
                      return "Insira seu peso";
                    }
                    return null;
                  },
                ),
                TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: const InputDecoration(
                      labelText: "Altura CM",
                      labelStyle: TextStyle(
                        color: Colors.blueAccent,
                        fontSize: 24.0,
                      )),
                  textAlign: TextAlign.center,
                  style:
                      const TextStyle(color: Colors.blueAccent, fontSize: 24.0),
                  controller: heightController,
                  validator: (String? value) {
                    if (value != null && value.isEmpty) {
                      return "Insira sua Altura";
                    }
                    return null ;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                  child: Container(
                    height: 48.0,
                    child: RaisedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          _calcular();
                        }
                      },
                      child: const Text(
                        "Calcular",
                        style: TextStyle(color: Colors.white, fontSize: 24.0),
                      ),
                      color: Colors.blueAccent,
                    ),
                  ),
                ),
                Text(
                  _infotext,
                  textAlign: TextAlign.center,
                  style: const TextStyle(color: Colors.blue, fontSize: 24),
                ),
              ],
            ),
          )),
    );
  }
}
