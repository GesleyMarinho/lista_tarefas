import 'package:buscador_gifs/UI/gif_page.dart';
import 'package:flutter/material.dart';
import 'package:buscador_gifs/UI/home_page.dart';

void main() {
  runApp( MaterialApp(
    debugShowCheckedModeBanner: false,
    //home: GifPage (),
    home:  const Home(),
    theme: ThemeData(hintColor: Colors.white),
  ));
}
